import numpy as np

N = 15
B = 5
outerdistance=1.0
innerdistance=0.2
radwidth = 2.0
distanceBetweenOuterStrengtheningExtraEdges = 3 #at least 2 to make any sense
smallerHoleStabilisatorSkipDistance = int(N/2)


wheel_file = open(r"wheel.off", "w")

wheel_file.write("OFF\n")
wheel_file.write(str(N*B*2)+" "+str(N*(B-1)*4+4*N)+" "+str(0)+"\n")

#outer vertices
for b in range(B):
  for n in range(N):
    wheel_file.write('%f'%(np.cos(2*np.pi*n/N)*outerdistance)+" "+'%f'%(np.sin(2*np.pi*n/N)*outerdistance)+" "+'%f'%(-radwidth/(B-1)*b+radwidth/2)+"\n")

#inner vertices
for b in range(B):
  for n in range(N):
    wheel_file.write('%f'%(np.cos(2*np.pi*n/N)*innerdistance)+" "+'%f'%(np.sin(2*np.pi*n/N)*innerdistance)+" "+'%f'%(-radwidth/(B-1)*b+radwidth/2)+"\n")

#outer faces
for b in range(B-1):
  for n in range(N):
    wheel_file.write("3 " + str(n+b*N) + " " + str(n+b*N+N) + " " + str(((n+1)%N+N)+b*N)+"\n")
    wheel_file.write("3 " + str(((n+1)%N+N)+b*N) + " " + str(((n+1)%N)+b*N) + " " + str(n+b*N)+"\n")

#inner faces
for b in range(B-1):
  for n in range(N):
    wheel_file.write("3 " + str(((n+1)%N+N)+b*N+B*N) + " " + str(n+b*N+N+B*N) + " " + str(n+b*N+B*N) +"\n")
    wheel_file.write("3 " + str(n+b*N+B*N) + " " + str(((n+1)%N)+b*N+B*N) + " " + str(((n+1)%N+N)+b*N+B*N) +"\n")

#first side
for n in range(N):
  wheel_file.write("3 " + str(n) + " " + str((n+1)%N) + " " + str(n+N*B)+"\n")
  wheel_file.write("3 " + str((n+1)%N) + " " + str(((n+1)%N+N*B)) + " " + str(n+N*B)+"\n")

"""
#first B-1 sides
for b in range(B-1):
  for n in range(N):
    #also cool
    #wheel_file.write("3 " + str(n+b*N) + " " + str(((n+1)%N+N)+b*N) + " " + str(n+b*N+N*B)+"\n")
    #wheel_file.write("3 " + str(((n+1)%N+N)+b*N) + " " + str(((n+1)%N+N*B)+b*N) + " " + str(n+b*N+N*B)+"\n")
    wheel_file.write("3 " + str(n+b*N) + " " + str((n+1)%N+b*N) + " " + str(n+b*N+N*B)+"\n")
    wheel_file.write("3 " + str((n+1)%N+b*N) + " " + str(((n+1)%N+N*B)+b*N) + " " + str(n+b*N+N*B)+"\n")
"""
#last side
for n in range(N):
  wheel_file.write("3 " + str(n+(B-1)*N+N*B) + " " + str((n+1)%N+(B-1)*N) + " " + str(n+(B-1)*N) +"\n")
  wheel_file.write("3 " + str(n+(B-1)*N+N*B) + " " + str(((n+1)%N+N*B)+(B-1)*N) + " " + str((n+1)%N+(B-1)*N) +"\n")

wheel_file.close()











visualWheel_file = open(r"visualWheel.off", "w")

visualWheel_file.write("OFF\n")
visualWheel_file.write(str(N*B*2+1)+" "+str(N*(B-1)*4+4*N+3*N*B)+" "+str(0)+"\n")

#outer vertices
for b in range(B):
  for n in range(N):
    visualWheel_file.write('%f'%(np.cos(2*np.pi*n/N)*outerdistance)+" "+'%f'%(np.sin(2*np.pi*n/N)*outerdistance)+" "+'%f'%(-radwidth/(B-1)*b+radwidth/2)+"\n")

#inner vertices
for b in range(B):
  for n in range(N):
    visualWheel_file.write('%f'%(np.cos(2*np.pi*n/N)*innerdistance)+" "+'%f'%(np.sin(2*np.pi*n/N)*innerdistance)+" "+'%f'%(-radwidth/(B-1)*b+radwidth/2)+"\n")

#testvertice
visualWheel_file.write(str(0)+" "+str(0)+" "+str(0)+"\n") #vertice index = 2*N*B

#outer faces
for b in range(B-1):
  for n in range(N):
    visualWheel_file.write("3 " + str(n+b*N) + " " + str(n+b*N+N) + " " + str(((n+1)%N+N)+b*N)+"\n")
    visualWheel_file.write("3 " + str(((n+1)%N+N)+b*N) + " " + str(((n+1)%N)+b*N) + " " + str(n+b*N)+"\n")

#inner faces
for b in range(B-1):
  for n in range(N):
    visualWheel_file.write("3 " + str(((n+1)%N+N)+b*N+B*N) + " " + str(n+b*N+N+B*N) + " " + str(n+b*N+B*N) +"\n")
    visualWheel_file.write("3 " + str(n+b*N+B*N) + " " + str(((n+1)%N)+b*N+B*N) + " " + str(((n+1)%N+N)+b*N+B*N) +"\n")

#first side
for n in range(N):
  visualWheel_file.write("3 " + str(n) + " " + str((n+1)%N) + " " + str(n+N*B)+"\n")
  visualWheel_file.write("3 " + str((n+1)%N) + " " + str(((n+1)%N+N*B)) + " " + str(n+N*B)+"\n")

"""
#first B-1 sides
for b in range(B-1):
  for n in range(N):
    #also cool
    #visualWheel_file.write("3 " + str(n+b*N) + " " + str(((n+1)%N+N)+b*N) + " " + str(n+b*N+N*B)+"\n")
    #visualWheel_file.write("3 " + str(((n+1)%N+N)+b*N) + " " + str(((n+1)%N+N*B)+b*N) + " " + str(n+b*N+N*B)+"\n")
    visualWheel_file.write("3 " + str(n+b*N) + " " + str((n+1)%N+b*N) + " " + str(n+b*N+N*B)+"\n")
    visualWheel_file.write("3 " + str((n+1)%N+b*N) + " " + str(((n+1)%N+N*B)+b*N) + " " + str(n+b*N+N*B)+"\n")
"""
#last side
for n in range(N):
  visualWheel_file.write("3 " + str(n+(B-1)*N+N*B) + " " + str((n+1)%N+(B-1)*N) + " " + str(n+(B-1)*N) +"\n")
  visualWheel_file.write("3 " + str(n+(B-1)*N+N*B) + " " + str(((n+1)%N+N*B)+(B-1)*N) + " " + str((n+1)%N+(B-1)*N) +"\n")


for n in range(N):
  for b in range(B):
    visualWheel_file.write("3 " + str(n+b*N)+" "+str(n+N*B+N*(B-1)-b*N)+" "+ str(2*N*B) + "\n")


for b in range(B):
  for n in range(N):
    visualWheel_file.write("3 " + str(n+b*N)+" "+str((n+distanceBetweenOuterStrengtheningExtraEdges)%N+b*N)+" "+ str(2*N*B) + "\n")

for b in range(B):
  for n in range(N):
    visualWheel_file.write("3 " + str(n+b*N+N*B)+" "+str((n+smallerHoleStabilisatorSkipDistance)%N+b*N+N*B)+" "+ str(2*N*B) + "\n")

visualWheel_file.close()












edges_file = open(r"wheel.xtraedges", "w")
edges_file.write(str(3*N*B)+"\n");

for n in range(N):
  for b in range(B):
    edges_file.write(str(n+b*N)+" "+str(n+N*B+N*(B-1)-b*N)+"\n")


for b in range(B):
  for n in range(N):
    edges_file.write(str(n+b*N)+" "+str((n+distanceBetweenOuterStrengtheningExtraEdges)%N+b*N)+"\n")

for b in range(B):
  for n in range(N):
    edges_file.write(str(n+b*N+N*B)+" "+str((n+smallerHoleStabilisatorSkipDistance)%N+b*N+N*B)+"\n")


edges_file.close()