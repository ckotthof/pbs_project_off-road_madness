#include "MSSSim.h"
#include <omp.h>

/////////////////////////////////////
/////////////// EX 5 ////////////////
/////////////////////////////////////

unsigned positivemodulo(int value, unsigned m) {
	int mod = value % (int)m;
	if (mod < 0) {
		mod += m;
	}
	return mod;
}

bool MSSSim::advance() {
	//m_collisionDetection.computeCollisionDetection(m_broadPhaseMethod, m_narrowPhaseMethod, m_eps);

	for (int obj = 1; obj < m_objects.size(); obj++) {
		Eigen::MatrixXd V, V_new;
		Eigen::MatrixXi F;
		(&m_objects[obj])->getMesh(V, F);
		V_new = V;

		std::vector<Eigen::Vector3d> f(V.rows(), Eigen::Vector3d::Zero());

		////
		// TODO: update V_new (position) with f (force)
		// use (m_edges[obj]) and (m_lengths[obj]) to get info. about springs as below
		// e.g., int a = (m_edges[obj])(i, 0); int b = (m_edges[obj])(i, 1); Eigen::Vector3d v0 = V.row(a);
		// when each vertex is lower than 0 (i.e., floor), compute penalty force with m_floor_stiffness
		std::vector<Eigen::Vector3d> fint(V.rows(), Eigen::Vector3d::Zero());
		std::vector<Eigen::Vector3d> fdamp(V.rows(), Eigen::Vector3d::Zero());
		std::vector<Eigen::Vector3d> fext(V.rows(), Eigen::Vector3d::Zero());

		//acceleration of wheel


		if (obj == 1) {
			int N_wheel = 15;
			int B_wheel = 5;
			for (int b = 0; b < 2 * B_wheel; b++) {
				for (int n = 0; n < N_wheel; n++) {
					fint[b * N_wheel + n] = (V.row(b * N_wheel + positivemodulo(n - 1, N_wheel)) - V.row(b * N_wheel + (n + 1) % N_wheel)).normalized() * accForce;

				}
			}
		}

		for (int i = 0; i < (m_edges[obj]).rows(); i++) {
			int a = (m_edges[obj])(i, 0);
			int b = (m_edges[obj])(i, 1);

			Eigen::Vector3d dir = V.row(a) - V.row(b);
			double len = dir.norm();
			dir.normalize();
			fint[a] += m_spring.stiffness * ((m_lengths[obj])[i] - len) * dir;
			fint[b] += m_spring.stiffness * ((m_lengths[obj])[i] - len) * -dir;
		}

		if (obj == 1 && hasExtraEdges && useExtraEdges)
			for (int i = 0; i < m_EXTRAedges.rows(); i++) {
				int a = m_EXTRAedges(i, 0);
				int b = m_EXTRAedges(i, 1);

				Eigen::Vector3d dir = V.row(a) - V.row(b);
				double len = dir.norm();
				dir.normalize();
				fint[a] += m_spring.stiffness * (m_EXTRAlengths[i] - len) * dir;
				fint[b] += m_spring.stiffness * (m_EXTRAlengths[i] - len) * -dir;
			}

		for (int i = 0; i < V.rows(); i++) {
			fext[i] = (&m_objects[obj])->getMass() * m_gravity;
			int pos1 = V(i, 0) + trackoffset >= 0 ? V(i, 0) + trackoffset : V(i, 0) - 1 + trackoffset;
			int pos2 = pos1 + 1;
			double heightPos1 = m_Vorig[0](pos1, 1);
			double heightPos2 = m_Vorig[0](pos2, 1);
			double vi0 = V(i, 0);
			double distToPos1 = V(i, 0) >= 0 ? V(i, 0) - (int)V(i, 0) : 1.0 - ((int)V(i, 0) - V(i, 0));
			double mapHeightAtPosition = heightPos1 * (1 - distToPos1) + heightPos2 * distToPos1;
			int trckoffset = trackoffset;

			if (V(i, 1) < mapHeightAtPosition) { //no bounds checking
				fext[i].y() -= m_floor_stiffness * (V(i, 1) - mapHeightAtPosition);
			}

			fdamp[i] = m_spring.damping * (m_velocities[obj])[i];
		}


		for (int i = 0; i < V.rows(); i++) {
			//(m_velocities[obj])[i] += (&m_objects[obj])->getMassInv() * m_dt * (fint[i] + fext[i] - fdamp[i]);

				int pos1 = V(i, 0) + trackoffset >= 0 ? V(i, 0) + trackoffset : V(i, 0) - 1 + trackoffset;
				int pos2 = pos1 + 1;
				double heightPos1 = m_Vorig[1](pos1, 1);
				double heightPos2 = m_Vorig[1](pos2, 1);
				double vi0 = V(i, 0);
				double distToPos1 = V(i, 0) >= 0 ? V(i, 0) - (int)V(i, 0) : 1.0 - ((int)V(i, 0) - V(i, 0));
				double mapHeightAtPosition = heightPos1 * (1 - distToPos1) + heightPos2 * distToPos1;

				//if (V(i, 1) < mapHeightAtPosition) { //no bounds checking
				//(m_velocities[obj])[i].x() = 0;
				//(m_velocities[obj])[i].z() = 0;
				//}

				//V_new.row(i) = V.row(i) + m_dt * (m_velocities[obj])[i].transpose();
				//std::cout << (m_velocities[obj])[i];
				const double EPS = 0.001f;
				const float EPS2 = EPS * EPS;
				const int i_max = 10;
				double deltaT2 = m_dt * m_dt;
				Eigen::MatrixXd A = (&m_objects[obj])->getMass() * Eigen::Matrix3d::Identity() - (deltaT2 * m_spring.stiffness * Eigen::Matrix3d::Identity());
				Eigen::Vector3d b = ((&m_objects[obj])->getMass() * Eigen::Matrix3d::Identity() * (m_velocities[obj])[i]) + (m_dt * (fint[i] + fext[i] - fdamp[i]));
				float k = 0;
				(m_velocities_new[obj])[i] = (m_velocities[obj])[i];
				Eigen::Vector3d r = b - A * (m_velocities_new[obj])[i];
				Eigen::Vector3d d = r;
				Eigen::Vector3d q;
				float alpha_new = 0;
				float alpha = 0;
				float beta = 0;
				float delta_old = 0;
				float delta_new = r.dot(r);
				float delta0 = delta_new;

				while (k<i_max && delta_new > EPS2) {
					q = A * d;
					alpha = delta_new / d.dot(q);
					(m_velocities_new[obj])[i] = (m_velocities_new[obj])[i] + alpha * d;
					r = r - alpha * q;
					delta_old = delta_new;
					delta_new = r.dot(r);
					beta = delta_new / delta_old;
					d = r + beta * d;
					k++;
				}

				if (V(i, 1) < mapHeightAtPosition) { //no bounds checking
				(m_velocities_new[obj])[i].x() = 0;
				(m_velocities_new[obj])[i].z() = 0;
				}

				V_new.row(i) = V.row(i) + m_dt * (m_velocities_new[obj])[i].transpose();

				(m_velocities[obj])[i] = (m_velocities_new[obj])[i];

				}

				(&m_objects[obj])->setMesh(V_new, F);

				// advance m_time
				m_time += m_dt;
				m_step++;

				return false;
			//std::cout << (m_velocities[obj])[i];
		}
		
		(&m_objects[obj])->setMesh(V_new, F);
	}
    // advance m_time
    m_time += m_dt;
    m_step++;

    return false;
}