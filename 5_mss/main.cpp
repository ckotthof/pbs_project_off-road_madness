#include <igl/writeOFF.h>
#include <thread>
#include "Gui.h"
#include "Simulator.h"
#include "MSSSim.h"

/*
 * GUI for the spring simulation. This time we need additional paramters,
 * e.g. which integrator to use for the simulation and the force applied to the
 * canonball, and we also add some more visualizations (trajectories).
 */
class MSSGui : public Gui {
   public:
    // simulation parameters
    float m_dt = 5e-3;
    float m_mass = 1.0;
    float m_floor_stiffness = 3000;
    bool useExtraEdges = true;
    float accForce =  22.0;

    Spring m_spring;  // stores properties of a spring

    MSSSim *p_MSSSim = NULL;

    const vector<char const *> m_mesh_types = {"Wheel", "Wheel+Scene", "Cross"};
    int m_selected_mesh = 1;

    float m_eps = 1.0;
    int m_selectedBroadPhase = 1;
    const std::vector<char const *> m_broadphases = {"None", "AABB", "Own"};
    int m_selectedNarrowPhase = 1;
    const std::vector<char const *> m_narrowphases = {"Exhaustive", "Own"};

    MSSGui() {
        // initialize the spring to be used
        m_spring.stiffness = 5000.0;
        m_spring.damping = 2;

        p_MSSSim = new MSSSim();
        p_MSSSim->setSpring(m_spring);
        setSimulation(p_MSSSim);

        // show vertex velocity instead of normal
        callback_clicked_vertex =
            [&](int clickedVertexIndex, int clickedObjectIndex,
                Eigen::Vector3d &pos, Eigen::Vector3d &dir) {
                RigidObject &o = p_MSSSim->getObjects()[clickedObjectIndex];
                pos = o.getVertexPosition(clickedVertexIndex);
                dir = o.getVelocity(pos);
            };
        start();
    }

    virtual void updateSimulationParameters() override {
        // change all parameters of the simulation to the values that are set in
        // the GUI
        p_MSSSim->setTimestep(m_dt);
        p_MSSSim->setMass(m_mass);
        p_MSSSim->setSpring(m_spring);
        p_MSSSim->setMesh(m_selected_mesh);
        p_MSSSim->setFloorStiffness(m_floor_stiffness);
        p_MSSSim->setuseExtraEdges(useExtraEdges);
        p_MSSSim->setAccForce(accForce);

        // Collision detection
        p_MSSSim->setBroadPhaseMethod(m_selectedBroadPhase);
        p_MSSSim->setNarrowPhaseMethod(m_selectedNarrowPhase);
        p_MSSSim->setEps(m_eps);
    }

    virtual void clearSimulation() override {
    }

    virtual void drawSimulationParameterMenu() override {
        ImGui::Text("Object");
        ImGui::Combo("Mesh", &m_selected_mesh, m_mesh_types.data(), m_mesh_types.size());
        ImGui::InputFloat("Spring Stiffness", &m_spring.stiffness, 0, 0);
        ImGui::InputFloat("Mass", &m_mass, 0, 0);
        ImGui::InputFloat("AccForce", &accForce, 0, 0);
        ImGui::InputFloat("Damping", &m_spring.damping, 0, 0);
        ImGui::InputFloat("dt", &m_dt, 0, 0);
        ImGui::Text("Floor");
        ImGui::InputFloat("Floor Stiffness", &m_floor_stiffness, 0, 0);
        ImGui::Checkbox("Use extra edges", &useExtraEdges);
        ImGui::Text("Collision Detection");
        if (ImGui::Combo("Broadphase", &m_selectedBroadPhase,
                         m_broadphases.data(), m_broadphases.size())) {
            p_MSSSim->setBroadPhaseMethod(m_selectedBroadPhase);
        }
        if (ImGui::Combo("Narrowphase", &m_selectedNarrowPhase,
                         m_narrowphases.data(), m_narrowphases.size())) {
            p_MSSSim->setNarrowPhaseMethod(m_selectedNarrowPhase);
        }
        ImGui::InputFloat("eps", &m_eps, 0, 0);
    }
};

int main(int argc, char *argv[]) {
    // create a new instance of the GUI for the spring simulation
    new MSSGui();

    return 0;
}