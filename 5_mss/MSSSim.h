#include <igl/edges.h>
#include "CollisionDetection.h"
#include "Simulation.h"

using namespace std;

struct Spring {
   public:
    float stiffness;
    float damping;
};

/*
 * Simulation of a string with an attached mass.
 */
class MSSSim : public Simulation {
   public:
    MSSSim() : Simulation(), m_collisionDetection(m_objects)  { init(); }

    virtual void init() override {
        setMesh(1);

        m_dt = 5e-3;
        m_mass = 1.0;
        m_floor_stiffness = 1000.0;
        m_gravity << 0, -9.81, 0;

        // pass objects to collision detection
        m_collisionDetection.setObjects(m_objects); // not sure if this works in the current setting, the objects might not yet exist in m_objects?

        m_broadPhaseMethod = 0;
        m_narrowPhaseMethod = 0;
        m_eps = 1.0;

        reset();
    }

    virtual void resetMembers() override {
        srand(0);


        for (int obj = 0; obj < m_objects.size(); obj++) {
            (&m_objects[obj])->reset();
            Eigen::Vector3d starting_pos;
            if (obj == 0)
                starting_pos << 0, 0, 0;
            else if (obj==1)
                starting_pos << 0.5, 1, 0;
            else
                starting_pos << 10+rand()%15, 3+rand() % 6, rand() % 10-5;

            (&m_objects[obj])->setMass(m_mass);
            (&m_objects[obj])->setMesh(m_Vorig[obj].rowwise() + starting_pos.transpose(), m_Forig[obj]);
            m_velocities[obj] = std::vector<Eigen::Vector3d>(m_Vorig[obj].rows(), Eigen::Vector3d::Zero());
            m_velocities_new[obj] = std::vector<Eigen::Vector3d>(m_Vorig[obj].rows(), Eigen::Vector3d::Zero());
        }
    }

    virtual void updateRenderGeometry() override {
        for (int obj = 0; obj < m_objects.size(); obj++) {
            (&m_objects[obj])->getMesh(m_renderV[obj], m_renderF[obj]);
        }
    }

	virtual bool advance() override;



    virtual void renderRenderGeometry(igl::opengl::glfw::Viewer &viewer) override {
        //cout << m_objects.size() << "\n";
        
        for (size_t i = 0; i < m_objects.size(); i++) {
            RigidObject& o = m_objects[i];
            if (o.getID() < 0) {
                int new_id = 0;
                if (i > 0) {
                    new_id = viewer.append_mesh();
                    o.setID(new_id);
                }
                else {
                    o.setID(new_id);
                }

                size_t meshIndex = viewer.mesh_index(o.getID());
                
                //if (i >= m_objects.size()) {
                    //viewer.data_list[meshIndex].show_lines = true;
                    //viewer.data_list[meshIndex].show_faces = true;//false;
                //}
                //else {
                //    viewer.data_list[meshIndex].show_lines = false;
                //}
                viewer.data_list[meshIndex].set_face_based(true);
                viewer.data_list[meshIndex].point_size = 2.0f;
                viewer.data_list[meshIndex].clear();
            }

            size_t meshIndex = viewer.mesh_index(o.getID());


            (&m_objects[i])->getMesh(m_renderV[i], m_renderF[i]);
            assert(m_renderF[i].cols() == 3);
            viewer.data_list[meshIndex].set_mesh(m_renderV[i], m_renderF[i]);
            //viewer.data_list[meshIndex].compute_normals();

            //Eigen::MatrixXd color;
            //o.getColors(color);
            //viewer.data_list[meshIndex].set_colors(color);
        }

        //int i = viewer.data_list.size() - 3;
        //for (int ii = 0; ii < i; ii++)
        //    viewer.data_list.pop_back();
        
    }

    void findAndLoadExtraEdges(const std::string& file) {
        if (loadExtraEdges(file)) return;
        if (loadExtraEdges("data/" + file)) return;
        if (loadExtraEdges("../data/" + file)) return;
        if (loadExtraEdges("../../data/" + file)) return;
        if (loadExtraEdges("../../../data/" + file)) return;
        std::cout << "No extra edges found for file " << file << std::endl;
    }

    bool loadExtraEdges(const std::string& path) {
        bool succ = false;

        std::ifstream infile(path);
        if (!infile.good()) {
            return false;
        }

        const std::string xtraedges(".xtraedges");
        if (path.compare(path.size() - xtraedges.size(), xtraedges.size(), xtraedges) == 0) {
            FILE* off_file = fopen(path.c_str(), "r");
            if (NULL == off_file)
            {
                printf("IOError: %s could not be opened...\n", path.c_str());
                return false;
            }
            int numEdges = -1;
            char line[1000];
            fgets(line, 1000, off_file);
            sscanf(line, "%d", &numEdges);
            m_EXTRAedges.resize(numEdges, 2);
            for (int i = 0; i < numEdges; i++)
            {
                int e1, e2;
                fgets(line, 1000, off_file);
                sscanf(line, "%d %d", &e1, &e2);
                m_EXTRAedges(i, 0) = e1;
                m_EXTRAedges(i, 1) = e2;
            }
            succ = true;

            if (succ) {
                std::cout << "Reading xtraedges-file from " << path << " ..."
                    << std::endl;
            }
        }
        return succ;
    }

#pragma region SettersAndGetters
    void setMass(double m) { m_mass = m; }

    void setSpring(const Spring &s) {
        m_spring = s;
    }

    void setMesh(int mesh) {
        std::string path;
        m_velocities.clear(); //maximum 10 objects in scene inclusive wheel
        m_velocities_new.clear(); 
        m_lengths.clear();
        m_edges.clear();
        m_renderV.clear();
        m_Vorig.clear();
        m_renderF.clear();
        m_Forig.clear();
        for (int i = 0; i < 30; i++) {
            m_velocities.push_back(std::vector<Eigen::Vector3d>());
            m_velocities_new.push_back(std::vector<Eigen::Vector3d>());
            m_lengths.push_back(std::vector<double>());
            m_edges.push_back(Eigen::MatrixXi());
            m_renderV.push_back(Eigen::MatrixXd());
            m_Vorig.push_back(Eigen::MatrixXd());
            m_renderF.push_back(Eigen::MatrixXi());
            m_Forig.push_back(Eigen::MatrixXi());
        }

        m_lengths.reserve(10); //maximum 10 objects in scene inclusive wheel
        m_objects.clear();


        /////////////////////////////////////////////////////////////////////////////////////TRACK INDEX 0///////////////////////////////////////////////////////////////////////
        m_objects.push_back(RigidObject("../../../data/working_track.off"));
        (&m_objects[0])->getMesh(m_Vorig[0], m_Forig[0]);



        // update spring info.
        igl::edges(m_Forig[0], m_edges[0]);
        m_lengths[0].resize((m_edges[0]).size());
        for (int i = 0; i < (m_edges[0]).rows(); i++) {
            (m_lengths[0])[i] =
                (m_Vorig[0].row((m_edges[0])(i, 0)) - m_Vorig[0].row((m_edges[0])(i, 1)))
                .norm();
        }
        assert(m_Vorig[0].rows() % 2 == 0);
        track_heights.resize(m_Vorig[0].rows() / 2, 1);

        trackoffset = -m_Vorig[0](0, 0);

        for (int i = 0; i < m_Vorig[0].rows() / 2; i++) {
            track_heights(i, 0) = m_Vorig[0](i, 1) - 5;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////////////WHEEL INDEX 1///////////////////////////////////////////////////////////////////////

        switch (mesh) {
        case 0:
            path = "../../../data/wheel.off";
            break;
        case 1:
            path = "../../../data/wheel.off";
            break;
        case 2:
            path = "";
            break;
        }
        m_objects.push_back(RigidObject(path));

        (&m_objects[1])->getMesh(m_Vorig[1], m_Forig[1]);

        // update spring info.
        igl::edges(m_Forig[1], m_edges[1]);
        m_lengths[1].resize((m_edges[1]).size());
        for (int i = 0; i < (m_edges[1]).rows(); i++) {
            (m_lengths[1])[i] =
                (m_Vorig[1].row((m_edges[1])(i, 0)) - m_Vorig[1].row((m_edges[1])(i, 1)))
                .norm();
        }

        m_EXTRAedges.resize(1, 1);
        hasExtraEdges = false;
        findAndLoadExtraEdges(path.replace(path.end() - 4, path.end(), ".xtraedges"));
        if (m_EXTRAedges.size() > 1) {
            hasExtraEdges = true;

            m_EXTRAlengths.resize(m_EXTRAedges.size());
            for (int i = 0; i < m_EXTRAedges.rows(); i++) {
                m_EXTRAlengths[i] =
                    (m_Vorig[1].row(m_EXTRAedges(i, 0)) - m_Vorig[1].row(m_EXTRAedges(i, 1)))
                    .norm();
            }

            std::cout << "extra edges rows: " << m_EXTRAedges.rows() << "\n";
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        /////////////////////////////////////////////////////////////////////////////////////STONES INDEX 2-29///////////////////////////////////////////////////////////////////////

        for (int stones = 2; stones < 30; stones++) {
            m_objects.push_back(RigidObject("../../../data/stone1.off"));
            (&m_objects[stones])->getMesh(m_Vorig[stones], m_Forig[stones]);


            // update spring info.
            igl::edges(m_Forig[stones], m_edges[stones]);
            m_lengths[stones].resize((m_edges[stones]).size());
            for (int i = 0; i < (m_edges[stones]).rows(); i++) {
                (m_lengths[stones])[i] =
                    (m_Vorig[stones].row((m_edges[stones])(i, 0)) - m_Vorig[stones].row((m_edges[stones])(i, 1)))
                    .norm();
            }
        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }

    void setFloorStiffness(double s){
        m_floor_stiffness = s;
    }

    void setuseExtraEdges(bool s) {
        useExtraEdges = s;
    }
    void setAccForce(double s) {
        accForce = s;
    }

    void setBroadPhaseMethod(int m) { 
        m_broadPhaseMethod = m; 
    }

    void setNarrowPhaseMethod(int m) {
        m_narrowPhaseMethod = m;
    }

    void setEps(double eps) {
        m_eps = eps;
    }

#pragma endregion SettersAndGetters

   private:
    int m_method;  // id of integrator to be used (0: analytical, 1: explicit
                   // euler, 2: semi-implicit euler, 3: explicit midpoint, 4:
                   // implicit euler)
    double m_mass;
    double m_floor_stiffness;
    bool useExtraEdges;
    double accForce;
    bool modelScene;

    // Collision detection
    CollisionDetection m_collisionDetection;
    int m_broadPhaseMethod;
    int m_narrowPhaseMethod;
    double m_eps;

    Eigen::Vector3d m_gravity;

    Spring m_spring;
    std::vector<std::vector<double>> m_lengths;
    std::vector<std::vector<Eigen::Vector3d>> m_velocities;
    std::vector<std::vector<Eigen::Vector3d>> m_velocities_new;
    std::vector<Eigen::MatrixXi> m_edges;

    Eigen::MatrixXi m_EXTRAedges;
    std::vector<double> m_EXTRAlengths;
    bool hasExtraEdges = false;

    Eigen::MatrixXi track_heights;
    int trackoffset;


    std::vector<Eigen::MatrixXd> m_renderV, m_Vorig;  // vertex positions for rendering
    std::vector<Eigen::MatrixXi> m_renderF, m_Forig;  // face indices for rendering
};