#include "CollisionDetection.h"


void CollisionDetection::computeBroadPhase(int broadPhaseMethod) {
    // compute possible collisions
    m_overlappingBodys.clear();

	switch (broadPhaseMethod) {
	case 0: { // none
		for (size_t i = 0; i < m_objects.size(); i++) {
			for (size_t j = i + 1; j < m_objects.size(); j++) {
				m_overlappingBodys.push_back(std::make_pair(i, j));
			}
		}
		break;
	}

	case 1: {  // AABB
        // compute bounding boxes
        std::vector<AABB> aabbs(m_objects.size());
        for (size_t i = 0; i < aabbs.size(); i++) {
            aabbs[i].computeAABB(m_objects[i]);
        }
        for (size_t i = 0; i < m_objects.size(); i++) {
            for (size_t j = i + 1; j < m_objects.size(); j++) {
                // add pair of objects to possible collision if their
                // bounding boxes overlap
                if (aabbs[i].testCollision(aabbs[j])) {
                    m_overlappingBodys.push_back(std::make_pair(i, j));
                }
            }
        }
        break;
    }

	case 2: {
		// Sweep and prune (work in progress)
        std::vector<AABB> aabbs(m_objects.size());
        std::vector<size_t> indices;

        for (size_t i = 0; i < aabbs.size(); i++) {
            aabbs[i].computeAABB(m_objects[i]);
            indices.push_back(i);
        }

    //    auto compare_min_values = [aabbs](size_t &a, size_t &b) {
    //         return aabbs[a].getMinCoord().x() < aabbs[b].getMinCoord().x();
    //     };

    //     std::sort(indices.begin(), indices.end(), compare_min_values);


        for (size_t i = 0; i < m_objects.size(); i++) {
            for (size_t j = i + 1; j < m_objects.size(); j++) {
                bool overlap = true;
                for (size_t axis = 0; axis < 3; axis++) {
                    double min = aabbs[i].getMinCoord()(axis);
                    double max = aabbs[i].getMaxCoord()(axis);
                    double other_min = aabbs[j].getMinCoord()(axis);
                    double other_max = aabbs[j].getMaxCoord()(axis);

                    if (max < other_min || other_max < min) {
                        overlap = false;
                        break;
                    }
                }

                if (overlap) {
                    m_overlappingBodys.push_back(std::make_pair(i, j));
                }

            }
        }


        break;
    }

	}
}

Eigen::Vector3d CollisionDetection::gjkSupportFunction(const Eigen::MatrixXd& Va,
                                                       const Eigen::MatrixXd& Vb,
                                                       const Eigen::Vector3d& d,
                                                       double& vAIndex,
                                                       double& vBIndex) {
    double maxA = std::numeric_limits<double>::min();
    vAIndex = 0;
    for (size_t i = 0; i < Va.rows(); i++) {
        double temp = d.dot(Va.row(i));
        if (maxA < temp) {
            maxA = temp;
            vAIndex = i;
        }
    }

    double maxB = std::numeric_limits<double>::min();
    vBIndex = 0;
    for (size_t j = 0; j < Vb.rows(); j++) {
        double temp = (-d).dot(Vb.row(j));
        if (maxB < temp) {
            maxB = temp;
            vBIndex = j;
        }
    }

    return Va.row(vAIndex) - Vb.row(vBIndex);
}

void CollisionDetection::computeNarrowPhase(int narrowPhaseMethod) {
    switch (narrowPhaseMethod) {
    case 0: {
        // exhaustive
        // iterate through all pairs of possible collisions
        for (auto overlap : m_overlappingBodys) {
            std::vector<Contact> temp_contacts[2];
            // compute intersection of a with b first and intersectino
            // of b with a and store results in temp_contacts
            for (int switcher = 0; switcher < 2; switcher++) {
                RigidObject* a =
                    &m_objects[(!switcher) ? overlap.first
                                            : overlap.second];
                RigidObject* b =
                    &m_objects[(!switcher) ? overlap.second
                                            : overlap.first];

                Eigen::MatrixXd Va, Vb;
                Eigen::MatrixXi Fa, Fb;
                a->getMesh(Va, Fa);
                b->getMesh(Vb, Fb);

                // iterate through all faces of first object
                for (int face = 0; face < Fa.rows(); face++) {
                    // iterate through all edges of given face
                    for (size_t j = 0; j < 3; j++) {
                        int start = Fa(face, j);
                        int end = Fa(face, (j + 1) % 3);

                        // check if there is a collision
                        ContactType ct = isColliding(
                            Va.row(start), Va.row(end), Vb, Fb);

                        // find collision and check for duplicates
                        switch (ct) {
                            case ContactType::VERTEXFACE: {
                                auto ret = m_penetratingVertices.insert(
                                    Fa(face, j));
                                // if not already in set
                                if (ret.second) {
                                    Contact temp_col =
                                        findVertexFaceCollision(
                                            Va.row(Fa(face, j)), Vb,
                                            Fb);
                                    temp_col.a = a;
                                    temp_col.b = b;
                                    temp_col.type =
                                        ContactType::VERTEXFACE;
                                    temp_contacts[switcher].push_back(
                                        temp_col);
                                }
                                break;
                            }
                            case ContactType::EDGEEDGE: {
                                int orderedStart = std::min(start, end);
                                int orderedEnd = std::max(start, end);
                                auto ret = m_penetratingEdges.insert(
                                    std::make_pair(orderedStart,
                                                    orderedEnd));
                                // if not already in set
                                if (ret.second) {
                                    Contact temp_col =
                                        findEdgeEdgeCollision(
                                            Va.row(orderedStart),
                                            Va.row(orderedEnd), Vb, Fb);
                                    temp_col.a = a;
                                    temp_col.b = b;
                                    temp_col.type =
                                        ContactType::EDGEEDGE;
                                    temp_contacts[switcher].push_back(
                                        temp_col);
                                }
                                break;
                            }
                            case ContactType::NONE: {
                                break;
                            }
                        }
                    }
                }
            }

            // look for vertexFace
            bool found = false;
            for (int i = 0; i < 2; i++) {
                for (auto cont : temp_contacts[i]) {
                    if (cont.type == ContactType::VERTEXFACE) {
                        m_contacts.push_back(cont);
                        found = true;
                        break;
                    }
                }
                if (found) {
                    break;
                }
            }
            if (found) {
                continue;
            }

            // take single edgeedge if possible
            if (temp_contacts[0].size() > 0 &&
                temp_contacts[0].size() < temp_contacts[1].size()) {
                for (int i = 0; i < temp_contacts[0].size(); i++) {
                    m_contacts.push_back(temp_contacts[0][i]);
                }
            } else if (temp_contacts[1].size() > 0 &&
                        temp_contacts[0].size() >
                            temp_contacts[1].size()) {
                for (int i = 0; i < temp_contacts[1].size(); i++) {
                    m_contacts.push_back(temp_contacts[1][i]);
                }
            } else if (temp_contacts[0].size() > 0) {
                for (int i = 0; i < temp_contacts[0].size(); i++) {
                    m_contacts.push_back(temp_contacts[0][i]);
                }
            } else if (temp_contacts[1].size() > 0) {
                for (int i = 0; i < temp_contacts[1].size(); i++) {
                    m_contacts.push_back(temp_contacts[1][i]);
                }
            }
        }
        break;
    }

	case 1: {
        // GJK
        for (std::pair<size_t, size_t> overlap : m_overlappingBodys) {
            RigidObject * objA = &m_objects[overlap.first];
            RigidObject * objB = &m_objects[overlap.second];

            Eigen::MatrixXd Va, Vb;
            Eigen::MatrixXi Fa, Fb;
            objA->getMesh(Va, Fa);
            objB->getMesh(Vb, Fb);

            double vAIndex, vBIndex;
            
            Eigen::Vector3d d = Eigen::Vector3d(0, 1, 0);
            std::vector<Eigen::Vector3d> simplex;
            Eigen::Vector3d p = gjkSupportFunction(Va, Vb, d, vAIndex, vBIndex);
            simplex.push_back(p);
            d = -d;

            while (true) {
                Eigen::Vector3d A = gjkSupportFunction(Va, Vb, d, vAIndex, vBIndex);
                if (A.dot(d) < 0) // No intersection
                    break;
            
                simplex.push_back(A);

                // Vector From origin to A
                Eigen::Vector3d AO = -A;

                if (simplex.size() == 2) {
                    // Line simplex
                    Eigen::Vector3d B = simplex[0];
                    
                    Eigen::Vector3d AB = B - A;
                    
                    if (AB.dot(AO) > 0) {
                        d = AB.cross(AO).cross(AB);
                        simplex = {A,B};
                    } else {
                        d = AO;
                        simplex = {A};
                    }
                } else if (simplex.size() == 3) {
                    // Triangle simplex
                    Eigen::Vector3d B = simplex[1];
                    Eigen::Vector3d C = simplex[0];

                    Eigen::Vector3d AB = B - A;
                    Eigen::Vector3d AC = C - A;

                    // Triangle Normal
                    Eigen::Vector3d ABC = AB.cross(AC);

                    if (ABC.cross(AC).dot(AO) > 0) {

                        if (AC.dot(AO) > 0) {
                            d = AC.cross(AO).cross(AC);
                            simplex = {A,C};
                        } else {
                            // star case
                            if (AB.dot(AO) > 0) {
                                d = AB.cross(AO).cross(AB);
                                simplex = {A,B};
                            } else {
                                d = AO;
                                simplex = {A};
                            }
                        }

                    } else {
                        if (AB.cross(ABC).dot(AO) > 0) {
                            // star case
                            if (AB.dot(AO) > 0) {
                                d = AB.cross(AO).cross(AB);
                                simplex = {A,B};
                            } else {
                                d = AO;
                                simplex = {A};
                            }

                        } else {
                            if (ABC.dot(AO) > 0) {
                                d = ABC;
                                simplex = {C,B,A};
                            } else {
                                d = -ABC;
                                simplex = {B,C,A};
                            }
                        }
                    }
                    // end triangle simplex

                } else if (simplex.size() == 4) {
                    // tetrahedron simplex
                    Eigen::Vector3d B = simplex[2];
                    Eigen::Vector3d C = simplex[1];
                    Eigen::Vector3d D = simplex[0];
                    Eigen::Vector3d AB = B - A;
                    Eigen::Vector3d AC = C - A;
                    Eigen::Vector3d AD = D - A;

                    // Normals of the three new faces
                    Eigen::Vector3d ABC = AB.cross(AC);
                    Eigen::Vector3d ACD = AC.cross(AD);
                    Eigen::Vector3d ADB = AD.cross(AB);

                    bool overABC = ABC.dot(AO) > 0;
                    bool overACD = ACD.dot(AO) > 0;
                    bool overADB = ADB.dot(AO) > 0;

                    if (overABC) {
                        simplex = {C,B,A};
                        d = ABC;
                        continue;
                    }
                    if (overACD) {
                        simplex = {D,C,A};
                        d = ACD;
                        continue;
                    }
                    if (overADB) {
                        simplex = {B,D,A};
                        d = ADB;
                        continue;
                    }

                    // Inside of tetrahedron: Collision
                    // Expanding Polytope Algorithm (EPA)
                    Eigen::Vector3d nextPoint;
                    Eigen::Vector3d normal;

                    std::vector<Eigen::Vector3i> faces = {
                        Eigen::Vector3i(0, 1, 2),
                        Eigen::Vector3i(0, 3, 1),
                        Eigen::Vector3i(0, 2, 3),
                        Eigen::Vector3i(1, 3, 2),
                    };

                    double depth = 0;
                    while (true) {
                        // Find closest triangle to the origin
                        double minDist = std::numeric_limits<double>::max();
                        for (size_t i = 0; i < faces.size(); i++) {
                            Eigen::Vector3d a = simplex[faces[i](0)];
                            Eigen::Vector3d b = simplex[faces[i](1)];
                            Eigen::Vector3d c = simplex[faces[i](2)];

                            Eigen::Vector3d ab = b - a;
                            Eigen::Vector3d ac = c - a;

                            Eigen::Vector3d faceNormal = ab.cross(ac).normalized();
                            double dist = a.dot(faceNormal);

                            if (dist < minDist) {
                                minDist = dist;
                                normal = faceNormal;
                            }
                        }

                        // Use the support function with normal of closest edge to
                        // find next point to be added to the expanding polytope 
                        Eigen::Vector3d temp = gjkSupportFunction(Va, Vb, d, vAIndex, vBIndex);

                        if (nextPoint != temp) {
                            nextPoint = temp;

                            // extending the polytope
                            std::vector<std::pair<int, int>> edges;
                            for (size_t i = 0; i < faces.size(); i++) {
                                Eigen::Vector3d a = simplex[faces[i](0)];
                                Eigen::Vector3d b = simplex[faces[i](1)];
                                Eigen::Vector3d c = simplex[faces[i](2)];

                                Eigen::Vector3d ab = b - a;
                                Eigen::Vector3d ac = c - a;
                                Eigen::Vector3d ad = d - a;

                                Eigen::Vector3d faceNormal = ab.cross(ac);

                                // Is the normal pointing towards the extend point?
                                if (faceNormal.dot(nextPoint) > 0) {

                                    for (size_t j = 0; j < 3; j++) {
                                        int start = faces[i](j);
                                        int end = faces[i]((j+1) % 3);

                                        // is this edge already in the edges vector?
                                        auto it = std::find(edges.begin(), edges.end(), std::make_pair(end, start));
                                        if (it != edges.end()) {
                                            edges.erase(it);
                                        } else {
                                            edges.push_back(
                                                std::make_pair(start, end)
                                            );
                                        }
                                    }

                                    faces.erase(faces.begin() + i);
                                }
                            }

                            simplex.push_back(nextPoint);
                            for (size_t i = 0; i < edges.size(); i++) {
                                Eigen::Vector3i face = Eigen::Vector3i(edges[i].first, edges[i].second, simplex.size() -1);
                                faces.push_back(face);
                            }

                        } else {
                            depth = nextPoint.dot(normal);
                            // Minimum translation vector
                            Eigen::Vector3d mtv = normal * depth;

                            std::cout << "Depth: " << depth << std::endl;
                            std::cout << "Minimal Translation Vector" << std::endl;
                            std::cout << mtv << std::endl;
                            std::cout << "Normal" << std::endl;
                            std::cout << normal << std::endl;
                            break;
                        }
                    }
                    break;
                }
            }
        }
		break;
       
        }
    }
}

void CollisionDetection::applyImpulse(double eps) {
    // compute impulse for all contacts
    for (auto contact : m_contacts) {
        Eigen::Vector3d vrel_vec = contact.a->getVelocity(contact.p) -
                                    contact.b->getVelocity(contact.p);
        double vrel = contact.n.dot(vrel_vec);
        if (vrel > 0) {
            // bodies are moving apart
            continue;
        }

        double ma_inv = contact.a->getMassInv();
        double mb_inv = contact.b->getMassInv();

        Eigen::Matrix3d Ia_inv = contact.a->getInertiaInvWorld();
        Eigen::Matrix3d Ib_inv = contact.b->getInertiaInvWorld();

        Eigen::Vector3d ra = contact.p - contact.a->getPosition();
        Eigen::Vector3d rb = contact.p - contact.b->getPosition();

        Eigen::Vector3d n = contact.n;

        Eigen::Vector3d numerator = -(1+eps)*vrel_vec;
        double denominator = ma_inv + mb_inv +
            n.dot((Ia_inv*(ra.cross(n))).cross(ra)) +
            n.dot((Ib_inv*rb.cross(n)).cross(rb));

        Eigen::Vector3d j = numerator / denominator;

        Eigen::Vector3d force = j.dot(n) * n;

        contact.a->setLinearMomentum(contact.a->getLinearMomentum() + force);
        contact.b->setLinearMomentum(contact.b->getLinearMomentum() - force);

        contact.a->setAngularMomentum(contact.a->getAngularMomentum() + ra.cross(force));
        contact.b->setAngularMomentum(contact.b->getAngularMomentum() - rb.cross(force));
    }
}